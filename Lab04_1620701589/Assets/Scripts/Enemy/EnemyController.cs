﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private float chasingThresholdDistance;
        
        private void Update()
        {
            MoveToPlayer();
            enemySpaceship.Fire();
        }

         private void MoveToPlayer()
         {
             //get enemy current position
             Vector2 position = transform.position;
             
             //Compute the enemy new position
             position = new Vector2(position.x, position.y - enemySpaceship.Speed * Time.deltaTime);
             
             //update the enemy position 
             transform.position = position;

             Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
             if (transform.position.y < min.y)
             {
                 Destroy(gameObject);
             }
         }
    }    
}

