﻿using System;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private ScoreManager scoreManager;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;
        [SerializeField] private int enemyInThisLevel;
        private int currentEnemyInThisLevel;
        private int currentScore;
        private int newScore;
        private float maxSpawnRateInSeconds = 5f;

        private void Start()
        {
            Invoke("SpawnEnemySpaceship",maxSpawnRateInSeconds);
            
            //Increase Spawn Rate Every 30 Seconds
            InvokeRepeating("IncreaseSpawnRate",0f,30f);
        }

        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
            
            startButton.onClick.AddListener(OnStartButtonClicked);
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }

        private void StartGame()
        {
            scoreManager.Init(this);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
            currentEnemyInThisLevel = enemyInThisLevel;
            currentScore = 0;
            newScore = 0;
        }
        
        private void SpawnPlayerSpaceship()
        {
            var spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
            
            Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
            
            var spaceship = Instantiate(enemySpaceship);
            spaceship.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);
            
            spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spaceship.OnExploded += OnEnemySpaceshipExploded;
            
            //Schedule when to spawn next enemy
            ScheduleNextEnemySpawn();
        }

        private void ScheduleNextEnemySpawn()
        {
            float spawnInSeconds;

            if (maxSpawnRateInSeconds > 1f)
            {
                //pick a number between 1 and maxSpawnRateInSeconds
                spawnInSeconds = Random.Range(1f, maxSpawnRateInSeconds);
            }
            else
                spawnInSeconds = 1f;
            Invoke("SpawnEnemySpaceship",spawnInSeconds);
        }

        private void IncreaseSpawnRate()
        {
            if (maxSpawnRateInSeconds > 1f)
                maxSpawnRateInSeconds--;
            if (maxSpawnRateInSeconds == 1f)
                CancelInvoke("IncreaseSpawnRate");        
                
        }
        
        private void AllDead()
        {
            CancelInvoke("SpawnEnemySpaceship");
            Restart();
        }

        private void OnEnemySpaceshipExploded()
        {
            currentScore = 10;
            newScore = currentScore + newScore;
            scoreManager.SetScore(newScore);
            currentEnemyInThisLevel--;

            if (currentEnemyInThisLevel <= 0) 
            {
                AllDead();
            }
        }

        private void Restart()
        {
            DestroyRemainingShip();
            dialog.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }

        private void DestroyRemainingShip()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }
            
            var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayer)
            {
                Destroy(player);
            }
        }

    }
}
