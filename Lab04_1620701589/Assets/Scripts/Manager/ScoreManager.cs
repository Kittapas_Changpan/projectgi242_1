﻿using System;
using Spaceship;
using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI finalScoreText;

        private GameManager gameManager;
        private int playerScore;
        

        public void Init(GameManager gameManager)
        {
            this.gameManager = gameManager;
            this.gameManager.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore(0);
        }
        
        public void SetScore(int score)
        {
            scoreText.text = $"Score : {score}";
            playerScore = score;
        }
        
        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot null");
        }
        
        private void OnRestarted()
        {
            finalScoreText.text = $"Your Score : {playerScore}";
            gameManager.OnRestarted -= OnRestarted;
            HideScore(true);
            playerScore = 0;
            SetScore(0);
        }

        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }
    }
}


